﻿using System;
using System.Collections.Generic;
using System.Text;

namespace haschode2020
{
    interface CompetitionData
    {
        List<Library> libraries { get; set; }
        List<Book> allBooks { get; set; }
        int daysForScanning { get; set; }
    }
}

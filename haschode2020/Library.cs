﻿using System;
using System.Collections.Generic;
using System.Text;

namespace haschode2020
{
    interface Library
    {
        int id { get; set; }
        List<Book> books { get; set; }
        int signUpTime { get; set; }
        int booksShippedPerDay { get; set; }
        Tuple<double, List<Book>> GetScore(int days, Dictionary<int, Book> scannedBooks);
    }
}

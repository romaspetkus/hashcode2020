﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace haschode2020
{
    class FileReader
    {
        private class CompetitionDataFile : CompetitionData
        {
            public List<Library> libraries { get; set; }
            public List<Book> allBooks { get; set; }
            public int daysForScanning { get; set; }

            public CompetitionDataFile(List<Library> libraries, List<Book> allBooks, int daysForScanning) 
            {
                this.libraries = libraries;
                this.allBooks = allBooks;
                this.daysForScanning = daysForScanning;
            }
        }

        private class BookFile : Book
        {
            public int score { get; set; }
            public int id { get; set; }

            public BookFile(int id, int score)
            {
                this.score = score;
                this.id = id;
            }
        }

        private class LibraryFile : Library
        {
            public int id { get; set; }
            public List<Book> books { get; set; }
            public int signUpTime { get; set; }
            public int booksShippedPerDay { get; set; }

            public LibraryFile(int id, int booksShippedPerDay, int signUpTime, List<Book> books) 
            {
                this.id = id;
                this.booksShippedPerDay = booksShippedPerDay;
                this.signUpTime = signUpTime;
                this.books = books;
            }

            Tuple<double, List<Book>> scoreCache;
            public Tuple<double, List<Book>> GetScore(int days, Dictionary<int, Book> scannedBooks)
            {
                //if (scoreCache != null) 
                //{
                //    return scoreCache;
                //}

                List<Book> booksToScan = new List<Book>();
                days -= signUpTime;

                double score = 0;

                int totalBooksToScanPossible = days * booksShippedPerDay;
                int booksCouldScan = 0;

                foreach (Book book in books)
                {
                    if (booksCouldScan >= totalBooksToScanPossible)
                    {
                        break;
                    }

                    Book lempineKnyga;
                    if (scannedBooks.TryGetValue(book.id, out lempineKnyga))
                    {
                        continue;
                    }

                    score += book.score;
                  
                    booksCouldScan++;
                    booksToScan.Add(book);
            
                    
                }

                double avgScore = score / booksCouldScan;
                scoreCache = new Tuple<double, List<Book>>(score - (signUpTime * 2 + days + (totalBooksToScanPossible - booksCouldScan) / booksShippedPerDay) * avgScore , booksToScan);
                return scoreCache;
            }

            public void sortBooks()
            {
                books.Sort((lhs, rhs) => rhs.score.CompareTo(lhs.score)); // sort by score
            }

            public int howMuchWillScan(int leftDays)
            {
                return (leftDays - signUpTime) * booksShippedPerDay;
            }
        }

        static string[] fileNames = { "a_example.txt", "b_read_on.txt", "c_incunabula.txt", /*"d_tough_choices.txt",*/ "e_so_many_books.txt", "f_libraries_of_the_world.txt" };
        public static List<CompetitionData> readFile(int hardnessLevel = 1)
        {
            List<CompetitionData> result = new List<CompetitionData>();
            for (int gg = 0; gg < hardnessLevel; gg++) 
            {
                using (StreamReader file = new StreamReader("data/" + fileNames[gg]))
                {
                    string ln;
                    ln = file.ReadLine();
                    string[] values = ln.Split(' ');
                    int bookCount = int.Parse(values[0]);
                    int libraryCount = int.Parse(values[1]);
                    int dayCount = int.Parse(values[2]);
                    ln = file.ReadLine();
                    values = ln.Split();
                    List<Book> books = new List<Book>();
                    for (int i = 0; i < bookCount; i++)
                    {
                        books.Add(new BookFile(i, int.Parse(values[i])));
                    }

                    List<Library> libraries = new List<Library>();
                    for (int i = 0; i < libraryCount; i++)
                    {
                        ln = file.ReadLine();
                        values = ln.Split();
                        int libraryBookCount = int.Parse(values[0]);
                        int signupDays = int.Parse(values[1]);
                        int shippingSpeed = int.Parse(values[2]);
                        List<Book> libraryBooks = new List<Book>();

                        ln = file.ReadLine();
                        values = ln.Split();
                        for (int j = 0; j < libraryBookCount; j++)
                        {
                            libraryBooks.Add(books[int.Parse(values[j])]);
                        }
                        var libraryToAdd = new LibraryFile(i, shippingSpeed, signupDays, libraryBooks);
                        libraryToAdd.sortBooks();
                        libraries.Add(libraryToAdd);

                    }

                    result.Add(new CompetitionDataFile(libraries, books, dayCount));
                }
            }
            return result;
        }

    }
}

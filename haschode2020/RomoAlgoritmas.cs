﻿using System;
using System.Collections.Generic;
using System.Text;

namespace haschode2020
{
    class RomoAlgoritmas
    {

        public static List<OutputLibrary> getResult2(CompetitionData data)
        {
            List<OutputLibrary> result = new List<OutputLibrary>();
            var leftLibraries = data.libraries;
            var daysLeft = data.daysForScanning;
            var scannedBooks = new Dictionary<int, Book>();
            Library maxLibrary = null;
            double maxScore = 0;
            List<Book> addedBooks = new List<Book>();

            bool sukam = true;
            while (sukam) 
            {
                 maxLibrary = null;
                 maxScore = 0;
                 addedBooks = new List<Book>();
                foreach (var library in leftLibraries)
                {
                    var score = library.GetScore(daysLeft, scannedBooks);
                    if (score.Item1 > maxScore)
                    {
                        maxScore = score.Item1;
                        maxLibrary = library;
                        addedBooks = score.Item2;
                    }
                }

                if (maxScore == 0) 
                {
                    break;
                }

               // Console.WriteLine(maxScore);
                daysLeft -= maxLibrary.signUpTime;
                foreach (var book in addedBooks)
                {
                    scannedBooks.Add(book.id, book);
                }

                leftLibraries.Remove(maxLibrary);
                result.Add(new OutputLibrary(maxLibrary.id, addedBooks.ConvertAll(x => x.id)));

            }
            return result;
        }

        public static List<OutputLibrary> getResult(CompetitionData data) 
        {
            var libraries = data.libraries;
            var scannedBooks = new Dictionary<int, Book>();
            libraries.Sort((lhs, rhs) => 
            {
                var result1 = rhs.GetScore(data.daysForScanning, scannedBooks);
                var result2 = rhs.GetScore(data.daysForScanning, scannedBooks);
                return result1.Item1.CompareTo(result2.Item1);
            }
            ); // sort by score

            int usedDays = 0;
            List<OutputLibrary> result = new List<OutputLibrary>();
            foreach (var library in libraries) 
            {
                if (library.signUpTime + 1 <= data.daysForScanning - usedDays) 
                {
                    var scannedBookCount = Math.Min((data.daysForScanning - usedDays - library.signUpTime - 1) * library.booksShippedPerDay, library.books.Count);
                    List<int> bookIds = library.books.GetRange(0, scannedBookCount).ConvertAll(x => x.id);
                    usedDays += library.signUpTime;
                    result.Add(new OutputLibrary(library.id, bookIds));
                    
                }
            }

            return result;
        }
    }
}

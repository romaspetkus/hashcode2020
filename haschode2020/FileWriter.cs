﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace haschode2020
{
    class FileWriter
    {
        public static void writeToFile(List<List<OutputLibrary>> outputLibrariesList)
        {
            int i = 0;
            foreach(List<OutputLibrary> outputLibraries in outputLibrariesList)
            {
                string fileName = String.Format("output_{0}.txt", i++);

                try
                {
                    // Check if file already exists. If yes, delete it.     
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }

                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        sw.WriteLine(outputLibraries.Count);

                        foreach (OutputLibrary lib in outputLibraries)
                        {
                            sw.WriteLine("{0} {1}", lib.id, lib.sortedBookIds.Count);
                            foreach (int bookId in lib.sortedBookIds)
                            {
                                sw.Write("{0} ", bookId);
                            }
                            sw.WriteLine();
                        }

                    }
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.ToString());
                }
            }
            
        }
    }

    class OutputLibrary
    {
        public int id { get; set; }

        public List<int> sortedBookIds { get; set; }

        public OutputLibrary(int id, List<int> sortedBookIds)
        {
            this.id = id;
            this.sortedBookIds = sortedBookIds;
        }
    }
}

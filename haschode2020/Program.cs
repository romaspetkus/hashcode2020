﻿using System;
using System.Collections.Generic;

namespace haschode2020
{
    class Program
    {
        static void Main(string[] args)
        {
            // max hardness level 6, reads all data files, min level 1
            List<CompetitionData> allData = FileReader.readFile(5);
            Console.WriteLine(allData.Count);
            var libs = new List<OutputLibrary>();
            //libs.Add(new OutputLibrary(0, new List<int>() { 0, 1, 2 }));
            //libs.Add(new OutputLibrary(1, new List<int>() { 1, 1, 3 }));

            List<List<OutputLibrary>> result = new List<List<OutputLibrary>>();
            var count = 0;
            foreach (var data in allData)
            {
                Console.WriteLine("Reading data nr:" + count++);
                result.Add(RomoAlgoritmas.getResult2(data));
            }

            FileWriter.writeToFile(result);

            //libs =;

            //var libs2 = new List<OutputLibrary>();
            //libs2.Add(new OutputLibrary(0, new List<int>() { 1, 1, 2 }));
            //libs2.Add(new OutputLibrary(1, new List<int>() { 1, 1, 3 }));

            //new List<List<OutputLibrary>>() { libs, libs2 });
        }
    }
}
